package br.com.cru.controllers;

import br.com.cru.fila.Queue;
import br.com.cru.models.Filme;

public class FilmeController {
    public static Queue queue;
    
    public FilmeController() {
        queue = new Queue();
    }
    
    public FilmeController(int qtd) {
        queue = new Queue(qtd);
    }
    
    public void cadastrar(Filme filme) {
        queue.enqueue(filme);
    }
    
    public String removeTopAction() {
        while (!((Filme)queue.peek()).getGenero().toLowerCase().equals("ação")){
            queue.dequeue();
        }
        return queue.peek().toString();
    }
    
    public String show() {
        return queue.toString();
    }
}
